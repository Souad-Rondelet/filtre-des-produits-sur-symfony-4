<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
// use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 


class FormController extends AbstractController
{
    /**
     * @Route("/form", name="form")
     * @return  Response
     */
    
    public function index(Request $request)
    {
      $product = new Product();
     

     $form = $this->createForm(ProductType::class, $product, [
        'action' =>$this->generateUrl('form')
      ]);

      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isSubmitted()){
        //saving info to data
        $entitymanager = $this->getDoctrine()->getManager();
        $entitymanager->persist($product);
        $entitymanager->flush();
        
   
        $this->addFlash(
          'info', 
          'Seance ajouter avec succès'
        );

        return $this->redirectToRoute('home_product');
      }

        return $this->render('form/index.html.twig', [
            'form_product' => $form->createView(),
        ]);
    }


 

}
