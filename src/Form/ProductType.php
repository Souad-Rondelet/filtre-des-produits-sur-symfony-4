<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'placeholder' =>'Inserez le nom de la seance',
                    'class' => 'custom_class'
                ]
            ])
            ->add('price', NumberType::class, [
                'attr' => [
                    'placeholder'=> 'inserez le prix'
                ]
            ])
            ->add('description', TextareaType::class, [
                'attr' => [
                    'placeholder' =>'inserez la description de la seance'
                ]
            ])
            ->add('image')
            ->add('contenu', TextType::class, [
                'attr' => [
                    'placeholder' => 'inserz le contenu de la seance'
                ]
            ])
            ->add('promo')
            ->add('categories')
            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
            
             
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
